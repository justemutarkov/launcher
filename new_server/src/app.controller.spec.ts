import {Test, TestingModule} from '@nestjs/testing';
import {EscapeFromTarkovController} from './escapeFromTarkov.controller';
import {AppService} from './app.service';

describe('EscapeFromTarkovController', () => {
    let appController: EscapeFromTarkovController;

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            controllers: [EscapeFromTarkovController],
            providers: [AppService],
        }).compile();

        appController = app.get<EscapeFromTarkovController>(EscapeFromTarkovController);
    });

    describe('root', () => {
        it('should return "Hello World!"', () => {
            expect(appController.getHello()).toBe('Hello World!');
        });
    });
});
