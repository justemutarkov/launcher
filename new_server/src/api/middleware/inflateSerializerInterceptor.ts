import {
  CallHandler,
  ClassSerializerInterceptor,
  ExecutionContext,
  Inject,
  Injectable,
  NestInterceptor,
  PlainLiteralObject,
} from '@nestjs/common';
import {ClassTransformOptions} from '@nestjs/common/interfaces/external/class-transform-options.interface';
import * as zlib from 'zlib';
import {Observable} from 'rxjs';
import {loadPackage} from '@nestjs/common/utils/load-package.util';
import {CLASS_SERIALIZER_OPTIONS} from '@nestjs/common/serializer/class-serializer.constants';
import {map} from 'rxjs/operators';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import {NextHandleFunction} from 'connect';
import {GenericClientResponseDto} from '../client/genericClientResponse.dto';

let classTransformer: any = {};

// NOTE (external)
// We need to deduplicate them here due to the circular dependency
// between core and common packages
const REFLECTOR = 'Reflector';

@Injectable()
export class InflateSerializerInterceptor implements NestInterceptor {

    private readonly bodyParser: NextHandleFunction;

    constructor(@Inject(REFLECTOR) protected readonly reflector: any) {
        classTransformer = loadPackage(
            'class-transformer',
            'ClassSerializerInterceptor',
            () => require('class-transformer'),
        );
        require('class-transformer');

        this.bodyParser = bodyParser.json({
            inflate: true,
            strict: true,
            type: () => true
        });
    }

    static async serialize(
        plainLiteralObject: PlainLiteralObject,
        options: ClassTransformOptions,
    ): Promise<Buffer> {
        const toSerialize = plainLiteralObject && plainLiteralObject.constructor !== Object
            ? classTransformer.classToPlain(plainLiteralObject, options)
            : plainLiteralObject;

        const genericResponse: GenericClientResponseDto = {
            err: 0,
            errmsg: null,
            data: toSerialize,
            crc: 0
        };

        return new Promise((resolve, reject) => {
            zlib.deflate(
                JSON.stringify(genericResponse),
                (error, result) => {
                    if (error) {
                        reject(error);
                    } else {
                        resolve(result);
                    }
                }
            );
        });
    }

    intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
        const options = this.getContextOptions(context);
        const response = context.switchToHttp().getResponse<express.Response>();
        return next
            .handle()
            .pipe(
                map(async (res: PlainLiteralObject) => {
                    response.setHeader('content-encoding', 'deflate');
                    response.end(await InflateSerializerInterceptor.serialize(res, options), 'binary');
                }),
            );
    }

    private getContextOptions(
        context: ExecutionContext,
    ): ClassTransformOptions | undefined {
        return (
            this.reflectSerializeMetadata(context.getHandler()) ||
            this.reflectSerializeMetadata(context.getClass())
        );
    }

    private reflectSerializeMetadata(
        obj: object | Function,
    ): ClassTransformOptions | undefined {
        return this.reflector.get(CLASS_SERIALIZER_OPTIONS, obj);
    }

}
