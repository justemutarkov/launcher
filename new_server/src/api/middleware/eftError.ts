export class EftError extends Error {

    constructor(public readonly errorCode: number, public readonly clientErrorMessage: string, message: string = clientErrorMessage) {
        super(message);
    }

}
