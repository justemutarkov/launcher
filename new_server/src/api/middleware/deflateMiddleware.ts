import {Injectable, Logger, NestMiddleware} from '@nestjs/common';
import * as express from 'express';
import {NextFunction} from 'express';
import {NextHandleFunction} from 'connect';
import bodyParser = require('body-parser');

@Injectable()
export class DeflateMiddleware implements NestMiddleware {

    private readonly logger = new Logger('LogApiCalls');
    private readonly bodyParser: NextHandleFunction;

    constructor() {
        this.bodyParser = bodyParser.json({
            inflate: true,
            strict: true,
            type: () => true
        });
    }

    async use(req: express.Request, res: express.Response, next: NextFunction): Promise<void> {
        this.logger.debug(`CLIENT CALLED [${req.method}] on ${req.originalUrl}`);
        req.headers['content-encoding'] = 'deflate';
        this.bodyParser(req, res, () => {
            next();
        });
    }

}
