import {EftError} from './eftError';
import {ArgumentsHost, Catch, ExceptionFilter, Injectable} from '@nestjs/common';
import {GenericClientResponseDto} from '../client/genericClientResponse.dto';
import * as zlib from 'zlib';
import * as  express from 'express';

@Injectable()
@Catch(EftError)
export class EftExceptionFilter implements ExceptionFilter {

    catch(exception: EftError, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<express.Response>();

        const errorResponse: GenericClientResponseDto = {
            err: exception.errorCode,
            errmsg: exception.clientErrorMessage,
            data: {},
            crc: 0
        };

        zlib.deflate(
            JSON.stringify(errorResponse),
            (error, result) => {
                if (error) {
                    response
                        .status(500)
                        .end('Serialization error');
                } else {
                    response.setHeader('content-encoding', 'deflate');
                    response.setHeader('content-type', 'application/json');
                    response
                        .status(200)
                        .end(result, 'binary');
                }
            }
        );
    }

}
