import {Controller, Logger, UseInterceptors} from '@nestjs/common';
import {InflateSerializerInterceptor} from '../middleware/inflateSerializerInterceptor';

@UseInterceptors(InflateSerializerInterceptor)
@Controller('client')
export class ItemsController {

    private readonly logger = new Logger(ItemsController.name);

}
