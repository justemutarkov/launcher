import {Body, Controller, Logger, Post, Request, Res, UseInterceptors} from '@nestjs/common';
import * as express from 'express';
import {LoginResponseDto} from './dto/loginResponse.dto';
import {LoginRequestDto} from './dto/loginRequest.dto';
import {InflateSerializerInterceptor} from '../../middleware/inflateSerializerInterceptor';
import {KeepAliveResponseDto} from './dto/keepAliveResponse.dto';

@UseInterceptors(InflateSerializerInterceptor)
@Controller('client/game')
export class GameController {

    private readonly logger = new Logger(GameController.name);

    @Post('keepalive')
    keepalive(): KeepAliveResponseDto {
        return {
            msg: 'OK'
        };
    }

    @Post('login')
    login(@Body() loginRequestDto: LoginRequestDto, @Request() request: express.Request, @Res() response: express.Response): LoginResponseDto {
        const backendUrl = `${request.protocol}://${request.get('host')}`;
        response.cookie('PHPSESSID', '098f6bcd4621d373cade4e832627b4f6');
        this.logger.debug(`Logging in with email = ${loginRequestDto.email} and password = ${loginRequestDto.pass}`);
        return {
            token: 'magic_token_here',
            aid: 1234567,
            lang: 'en',
            languages: {
                en: 'English'
            },
            ndaFree: true,
            queued: false,
            taxonomy: 341,
            activeProfileId: '098f6bcd4621d373cade4e832627b4f6',
            backend: {
                Main: backendUrl,
                Trading: backendUrl,
                Messaging: backendUrl,
                RagFair: backendUrl
            },
            utc_time: new Date().getTime(),
            totalInGame: 0,
            twitchEventMember: false
        };
    }

    @Post('version/validate')
    versionValidate(): null {
        return null;
    }

}

