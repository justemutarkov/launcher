import {Module} from '@nestjs/common';
import {AppService} from './app.service';
import {ClientModule} from './api/client/client.module';

@Module({
    imports: [ClientModule],
    controllers: [],
    providers: [AppService],
    exports: [],
})
export class AppModule {
}
