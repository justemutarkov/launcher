# JustEmuTarkov 0.7.4 - electron base

## Run the server

For development
```bash
cd new_server
yarn # installs dependencies
yarn start:dev
```

For production
```bash
cd new_server
yarn # installs dependencies
yarn build && yarn start:prod
```

# Creators:
 - TheMaoci
 - AmazingTurtle

