import './stylesheets/window.css';
import './stylesheets/main.css';
import {remote, shell} from 'electron';
import jetpack from 'fs-jetpack';
import {Index} from './ui/index.jsx';
import React from 'react';
import ReactDOM from "react-dom";

// Small helpers you might want to keep
//import "./helpers/context_menu.js";
//import "./helpers/external_links.js";

// ----------------------------------------------------------------------------
// Everything below is just to show you how it works. You can delete all of it.
// ----------------------------------------------------------------------------

const app = remote.app;
const appDir = jetpack.cwd(app.getAppPath());

// Holy crap! This is browser window with HTML and stuff, but I can read
// files from disk like it's node.js! Welcome to Electron world :)
const manifest = appDir.read('package.json', 'json');

const osMap = {
  win32: 'Windows',
  darwin: 'macOS',
  linux: 'Linux'
};

/* Window header title handling (close, minimalize) */
(function handleWindowControls() {
  // When document has loaded, initialise
  document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
      init();
    }
  };

  function init() {
    let window = remote.getCurrentWindow();
    const minButton = document.getElementById('min-button'),
      closeButton = document.getElementById('close-button');

    minButton.addEventListener('click', event => {
      window = remote.getCurrentWindow();
      window.minimize();
    });

    closeButton.addEventListener('click', event => {
      window = remote.getCurrentWindow();
      window.close();
    });

    document.addEventListener('click', function (event) {
      if (event.target.tagName === 'A' && event.target.href.startsWith('http')) {
        event.preventDefault();
        shell.openExternal(event.target.href);
      }
    });

    ReactDOM.render(<Index />, document.getElementById('react-container'));
  }
})();

/* END TITLE BUTTONS HANDLING */
