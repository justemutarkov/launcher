import React from 'react';
import {Settings} from './settings.jsx';
import './index.css';
import './stars.css';

const remote = require('electron').remote;
const {spawn} = remote.require('child_process');
const storage = remote.require('electron-json-storage');

export class Index extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      menu: undefined
    };
  }

  async getConfigKey(key) {
    return new Promise((resolve, reject) => {
      storage.get(key, function (err, data) {
        if (err) {
          reject(err);
        } else {
          resolve(data && data.value);
        }
      })
    })
  }

  async launchGame() {
    const gamePath = await this.getConfigKey('gamePath');

    const loginToken = {
      email: 'turtleonsteroids',
      password: 'iliketurtles',
      timestamp: Math.floor(new Date() / 1000) // unix timestamp
    };
    const serialized = JSON.stringify(loginToken);
    // const binaryRepresentation = Array.from(Buffer.from(serialized, 'utf-8').values());
    const hexRepresentation = Buffer.from(serialized, 'utf-8').toString('hex');

    const regAddProcess = spawn(
      'C:\\Windows\\System32\\reg.exe',
      [
        'add',
        'HKCU\\SOFTWARE\\Battlestate Games\\EscapeFromTarkov',
        '/v',
        'bC5vLmcuaS5u_h1472614626',
        '/t',
        'REG_BINARY',
        '/d',
        hexRepresentation,
        '/f'
      ]
    );

    regAddProcess.on('error', (err) => {
      alert('Unable to set registry keys');
    });
    regAddProcess.on('close', () => {
      const eftProcess = spawn(gamePath, {detached: true});
      eftProcess.on('error', (err) => {
        alert('Could not start the game');
      });
    });
  }

  selectMenu(menu) {
    this.setState({menu});
  }

  renderMenu() {
    if (this.state.menu === 'settings') {
      return <Settings/>;
    }
  }

  render() {
    return (
      <div className="index">
        <div className="background"/>
        <div id='stars'/>
        <div id='stars2'/>
        <div id='stars3'/>
        <div className="menu">
          <button onClick={() => this.launchGame()}>Launch Game</button>
          <button onClick={() => this.selectMenu('settings')}>Settings</button>
        </div>
        <div className="selectedMenuData">
          {this.renderMenu()}
        </div>
      </div>
    );
  }

}
