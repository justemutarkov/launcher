# Intercept traffic with fiddler script

## Enable HTTPS decryption

Go to `Tools > Options` and open the `HTTPS` tab. Check `Decrypt HTTPS traffic` and follow instructions provided by
fiddler.

## Modify Fiddler Script

Go to the `Fiddler Script` tab in the main view, it is between `Log` and `Fiddler Orchestra Beta`.

The following script modifications will add the `deflate` `Content-Encoding` header to requests and responses which have
Hostname set to `prod.escapefromtarkov.com` or `trading.escapefromtarkov.com`.

Add a new option to fiddler script in scope of `class Handlers`, so you can toggle if you wish to add headers or not:
```cs
public static RulesOption("Add EFT Headers")
var m_EscapeFromTarkovHeaders: boolean = true;
```

Add the following lines at the very end of the `OnBeforeRequest` function:
```cs
if (m_EscapeFromTarkovHeaders && (oSession.HostnameIs("prod.escapefromtarkov.com") || oSession.HostnameIs("trading.escapefromtarkov.com") || oSession.HostnameIs("ragfair.escapefromtarkov.com")) && !oSession.oRequest.headers.Exists("Content-Encoding"))
{
    oSession.oRequest.headers.Add("Content-Encoding", "deflate");
}
```

Add the following lines at the very end of the `OnBeforeResponse` function:
```cs
if (m_EscapeFromTarkovHeaders && (oSession.HostnameIs("prod.escapefromtarkov.com") || oSession.HostnameIs("trading.escapefromtarkov.com") || oSession.HostnameIs("ragfair.escapefromtarkov.com")) && !oSession.oResponse.headers.Exists("Content-Encoding"))
{
    oSession.oResponse.headers.Add("Content-Encoding", "deflate");
}
```

You can toggle the option in the toolbar under `Rules > Add EFT Headers`. By default it is set to `true`.